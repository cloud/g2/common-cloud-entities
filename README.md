# common-cloud-entities

Common code for *-cloud-entities repositories, namely:
* [prod-brno-cloud-entities](https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities)
* [prod-ostrava-cloud-entities](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities)


## openstack entities

Repository uses mostly direct terraform HCL syntax (`*.tf`) with exceptions for:
 * project-quota-acl
 * static-identity-mapping
which are used as a source and *.tf files are generated by python generator[s] in [ci/src/*.py](/ci/src/)

The reasons why are above entities stored in non-HCL syntax are:
 * more convenient format, user/machine easily generated
 * support for automatic openstack project lifecycle management (so projects may get expired w/o touching got repository)

Terraform state is currently managed by [ICS Gitlab, named `prod-ostrava`](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities/-/terraform), [ICS Gitlab, named `prod-brno`](https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/terraform) respectivelly.

This repository contains most important Openstack entities typically pre-created by cloud admin team like:
 * [domains `prod-ostrava`](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities/-/tree/master/environments/prod-ostrava/openstack/domains), [domains `prod-brno`](https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/tree/master/environments/prod-brno/openstack/domains)
 * [projects, quotas, ACLs `prod-ostrava`](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities/-/tree/master/environments/prod-ostrava/openstack/projects-quotas-acls), [projects, quotas, ACLs `prod-brno`](https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/tree/master/environments/prod-brno/openstack/projects-quotas-acls)
 * [ad-hoc users `prod-ostrava`](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities/-/tree/master/environments/prod-ostrava/openstack/users), [ad-hoc users `prod-brno`](https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/tree/master/environments/prod-brno/openstack/users) 
 * [identity mappings `prod-ostrava`](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities/-/tree/master/environments/prod-ostrava/openstack/global-static-identity-mappings), [identity mappings `prod-brno`](https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/tree/master/environments/prod-brno/openstack/global-static-identity-mappings)
 * [flavors `prod-ostrava`](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities/-/tree/master/environments/prod-ostrava/openstack/flavors), [flavors `prod-brno`](https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/tree/master/environments/prod-brno/openstack/flavors)
 * [networks `prod-ostrava`](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities/-/tree/master/environments/prod-ostrava/openstack/networks), [networks `prod-brno`](https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/tree/master/environments/prod-brno/openstack/networks)
 * [subnets `prod-ostrava`](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities/-/tree/master/environments/prod-ostrava/openstack/subnets), [subnets `prod-brno`](https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/tree/master/environments/prod-brno/openstack/subnets)
 * [routers `prod-ostrava`](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities/-/tree/master/environments/prod-ostrava/openstack/routers), [routers `prod-brno`](https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/tree/master/environments/prod-brno/openstack/routers)
 * [host aggregates `prod-ostrava`](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities/-/tree/master/environments/prod-ostrava/openstack/aggregates/), [routers `prod-brno`](https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/tree/master/environments/prod-brno/openstack/aggregates/)

Following entities are typically NOT included by purpose:
 * any user related entities
 * personal projects
 * auto-generated users (AAI data)

There may be exceptions, for instance when we want to disable personal project we can list it as disabled.

## Grafana monitoring UI entities

We maintain here only basic entities as:
 * data sources
 * folders
 * teams
 * users with higher role (not functional yet)

Following entities are typically NOT included by purpose:
 * dashboards (as we want to support in UI modification, revert to previous version will be possible via MariaDB revert or via import from [prod-ostrava-cloud-entities-dump repository](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities-dump), [prod-brno-cloud-entities-dump repository](https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities-dump) )

## Known Issues

### OpenStack Project renaming
Renaming of project leads to project entities re-creation (due to terraform cloud resources named after the project) leading to loss of project resources' effectivity => avoid renaming projects unless necessary. The only approach of re-gaining old cloud resources is based on changing recreated project ID in database to ID former project as shown below.
  * Effect of project recreation can be seen in pipeline https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/jobs/1937788#L434
  * SQL to change project ID (using keystone database): `update project set id='ORIGINAL_ID_WHERE_RESOURCES_BELONG' where id='ID_OF_NEWLY_CREATED_EMPTY_PROJECT';`.

Example: Original project (id: `1234`, name: `ABC`), renamed to (id: `1235`, name: `ABD`). To get back all project resources linked to project id `1234` you need to do:

```sql
use keystone;
SELECT * FROM project WHERE id="1235"
update project set id='1234' where id='1235'
```      

## FAQ
Following section is processed for [`prod-ostrava`](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities) but also valid for [`prod-brno`](https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities).

### Missing functionality

1. Grafana entities are not yet pushed from CI/CD pipeline as IP whitelisting on https://direct-grafana.ostrava.openstack.cloud.e-infra.cz does not work as needed

### How to import existing cloud object[s] into terraform state with CI/CD pipeline

Run [manual pipeline](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities/-/pipelines/new) with environment variable `TERRAFORM_IMPORT_ARGS`. If you need multiple imports at once you may use also variables `TERRAFORM_IMPORT1_ARGS` ... `TERRAFORM_IMPORT10_ARGS`.

Example:

In [pipeline#272895](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities/-/pipelines/272895)/[job#853229](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities/-/jobs/853229#L383) we saw conflict.
This means project `meta-cloud-kubernetes` already exists. To associate terraform resource `openstack_identity_project_v3` we run [pipeline#272904](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities/-/pipelines/272904) with following environment varible:

```sh
TERRAFORM_IMPORT_ARGS="openstack_identity_project_v3.einfra_cz_meta_cloud_kubernetes cb00fe018d2b494f86adb7ae80b7b545"
```
As part of [pipeline#272904](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities/-/pipelines/272904)/[job#853257](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities/-/jobs/853257#L217) we can see import was successful and we moved forward. Another multi-import example is [pipeline#287842/job#901091](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities/-/jobs/901091).

Note: currently only single import per CI/CD pipeline is supported.

### How to manipulate with terraform state from your machine

You should generally avoid it, but in some situations it may be handy to perform multiple actions at once.
When absolutely necessary follow CI/CD pipeline steps (`before_script` and deploy snippet yaml anchored as `&deploy-workflow` ).

Terraform backend environment variables are dumped every CI pipeline job, rest of credentials are stored in CI-CD variables.

### How to include sensitive data into the *openstack-entities repository

1. Define variable (as sensitive = true). Name of the variable should contain full path i.e. `<resource-type>_<resource-name>_<resource-field-name>`
1. Assign entity field with the variable (`field = var.<name-of-var>`)
1. Define secret in protected CI/CD variable `OSTACK_TF_ENV_VARS_FILE` (type file) for OpenStack terraform part and `GRAFANA_TF_ENV_VARS_FILE` (type file) for Grafana terraform part

Example: [commit/7e4b4ff9](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities/-/commit/7e4b4ff9).

### How to disable project keeping resources

* drop openstack mapping
* add `.project.enabled=false`

### How to deal with pipeline unable to acquire terraform state lock - terraform stale locks

If you encounter situation when Gitlab Terraform HTTP backend lock gets stale, as in [job #1073113](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities/-/jobs/1073113) i.e.
```console
...
INFO: Performing step plan (terraform plan --out=deploy.tfplan, log:20231013T101001-terraform-plan.log)
╷
│ Error: Error acquiring the state lock
│ 
│ Error message: HTTP remote state already locked:
│ ID=f2b030b0-8d74-afc4-9f7b-421e7fe5b7f0
│ Lock Info:
│   ID:        b83b38f5-2628-074a-b4af-dda1632af3f3
│   Path:      
│   Operation: OperationTypePlan
│   Who:       root@runner-ff44bedb-project-5290-concurrent-1
│   Version:   1.5.2
│   Created:   2023-10-13 10:10:01.353769935 +0000 UTC
│   Info:      
│ 
│ ...
```

You should launch manual CI pipeline job `ostack-entities-deploy-manual` (as [done here](https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/jobs/1292348)) with following environment configuration:
```
WORKFLOW_STEPS_DEPLOY_MANUAL="init,version,validate,providers,graph,state-list-if-exists,force-unlock"
TERRAFORM_LOCK_ID="f2b030b0-8d74-afc4-9f7b-421e7fe5b7f0"
```
Next CI pipeline should work as expected.

### How apply Terraform antities into empty OpenStack cloud

Assume you have entered situation when OpenStack DB is lost.

In such situation you will encounter *-cloud-entities pipeline [failing following way](https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/jobs/1327910):
 * conflicts on default openstack roles
 * conflicts on default (and einfra_cz) openstack domains
 * conflicts on default/admin openstack project

To solve this situation you should:
 * Save current state from [state-page](https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/terraform)
 * Verify state is properly stored and is not empty
 * Delete terraform cloud state (for instance `prod-brno`)
 * Launch manual job `ostack-entities-deploy-manual` [repetitively with following arguments](https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/jobs/1327945):
   * domains
     * `TERRAFORM_IMPORT1_ARGS = openstack_identity_project_v3.Default default`
     * `TERRAFORM_IMPORT2_ARGS = openstack_identity_project_v3.einfra_cz 3******************************5`
   * default/admin project
     * `TERRAFORM_IMPORT3_ARGS = openstack_identity_project_v3.Default_admin 2******************************0`
   * optionally various default roles, these should addressed already: [prod-brno](https://gitlab.ics.muni.cz/cloud/g2/prod-brno-cloud-entities/-/commit/8890590ea0b598aa357e0cfb3f9726b2ba129c33), [prod-ostrava](https://gitlab.ics.muni.cz/cloud/g2/prod-ostrava-cloud-entities/-/commit/f660d9e4b9364de92fe87351132a3898edfabfef)
     * `TERRAFORM_IMPORT4_ARGS = openstack_identity_role_v3.heat_stack_user 8******************************8`
     * `TERRAFORM_IMPORT5_ARGS = openstack_identity_role_v3.member 5******************************0`
     * `TERRAFORM_IMPORT6_ARGS = openstack_identity_role_v3.reader 4******************************9`
     * `TERRAFORM_IMPORT7_ARGS = openstack_identity_role_v3.admin c******************************1`

To see actual ids, you need to perform following commands as openstack super-admin:
```
openstack domain list
openstack project list
openstack role list
```
