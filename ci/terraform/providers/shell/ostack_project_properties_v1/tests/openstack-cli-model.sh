#!/bin/bash

# openstack fake file-based test model
# implemented invocations:
# a] ${OPENSTACK} project show ${ostack_project_id} -fjson
# b] ${OPENSTACK} project set --property "${i_ostack_project_property}=" "${ostack_project_id}"

set -eo pipefail

SCRIPT_DIR=$(dirname $(readlink -f $0))

function get_project_props() {
    local project_id="$1"

    if [ -s "${SCRIPT_DIR}/${project_id}.json" ]; then
        cat "${SCRIPT_DIR}/${project_id}.json"
    else
        echo "{\"project-id\":\"${project_id}\"}"
    fi
}

if [ "$1" == "project" -a "$2" == "show" ]; then
    project_id="$3"
    get_project_props "${project_id}"
elif [ "$1" == "project" -a "$2" == "set" -a "$3" == "--property" ]; then
    property_value_pair="$4"
    project_id="$5"
    property_name="$(echo "${property_value_pair/=/ }" | awk '{printf $1}')"
    property_value="$(echo "${property_value_pair/=/ }" | awk '{printf $2}')"

    merged_props="$(jq -n --argfile hash1 <(get_project_props "${project_id}") --argfile hash2 <(echo "{\"${property_name}\":\"${property_value}\"}") '$hash1 + $hash2')"
    echo "${merged_props}" > "${SCRIPT_DIR}/${project_id}.json"
else
    # not supported invocation
    exit 2
fi
