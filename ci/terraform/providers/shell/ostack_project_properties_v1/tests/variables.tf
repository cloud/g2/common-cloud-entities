
variable "project_abcd_property_enabled" {
  default = 0
  description = "project_abcd_property resource enable (0/1 ~ disabled/enabled)"
}

variable "project_abcd_properties" {
  default = "{}"
  description = "project abcd properties"
}


variable "project_abce_property_enabled" {
  default = 0
  description = "project abce property resource enable (0/1 ~ disabled/enabled)"
}

variable "project_abce_properties" {
  default = "{}"
  description = "project abce properties"
}

variable "project_abcf_property_enabled" {
  default = 0
  description = "project abcf property resource enable (0/1 ~ disabled/enabled)"
}

variable "project_abcf_properties" {
  default = "{}"
  description = "project abcf properties"
}
