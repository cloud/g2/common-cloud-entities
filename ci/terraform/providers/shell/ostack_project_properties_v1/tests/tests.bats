#!/usr/bin/env bats

load test-lib.sh

# Setup and Teardown Functions
# ###########################################################################
setup() {
    pushd "$BATS_TEST_DIRNAME"
}

teardown() {
    popd
}


# Prelude
# ###########################################################################
@test "environment is cleaned up (we are starting from scratch)" {
    rm -rf ./.terraform
    rm -f ./.terraform.lock.hcl ./terraform.tfstate ./plan ./1234-*.json ./1234-*.vars
}

@test "terraform client installed" {
    terraform --help
}

@test "terraform initialization succeeded" {
    terraform init
}

@test "terraform infrastructure declaration is valid (validated)" {
    terraform validate
}

# ###########################################################################
# Phase0
# ###########################################################################
@test "Phase0: terraform plan succeeded" {
    terraform plan --out plan
    test -f plan -a -s plan
}

@test "Phase0: terraform apply succeeded" {
    terraform apply plan
}

@test "Phase0: no properties produced yet" {
    assert_properties_files "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
}

# ###########################################################################
# Phase1
# ###########################################################################
@test "Phase1: terraform plan succeeded (added empty property hash { })" {
    rm -f plan
    terraform plan -var 'project_abcd_property_enabled=1' --out plan
    test -f plan -a -s plan
}

@test "Phase1: terraform apply succeeded" {
    terraform apply plan
}

@test "Phase1: abcd produced as empty, no change in the cloud" {
    assert_properties_files "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
}

# ###########################################################################
# Phase2
# ###########################################################################
@test "Phase2: terraform plan succeeded (added first supported property VO)" {
    rm -f plan
    terraform plan -var 'project_abcd_property_enabled=1' -var 'project_abcd_properties={"VO":"abcd"}' --out plan
    test -f plan -a -s plan
}

@test "Phase2: terraform apply succeeded" {
    terraform apply plan
}

@test "Phase2: abcd produced with VO property" {
    assert_properties_files "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
    assert_properties_contents "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
}

# ###########################################################################
# Phase3
# ###########################################################################
@test "Phase3: terraform plan succeeded (added another supported property accounting:VO)" {
    rm -f plan
    terraform plan -var 'project_abcd_property_enabled=1' -var 'project_abcd_properties={"VO":"abcd", "accounting:VO":"abcd"}' --out plan
    test -f plan -a -s plan
}

@test "Phase3: terraform apply succeeded" {
    terraform apply plan
}

@test "Phase3: abcd changed to contain VO and accounting:VO properties" {
    assert_properties_files "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
    assert_properties_contents "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
}

# ###########################################################################
# Phase4
# ###########################################################################
@test "Phase4: terraform plan succeeded (added another but un supported property)" {
    rm -f plan
    terraform plan -var 'project_abcd_property_enabled=1' -var 'project_abcd_properties={"VO":"abcd", "accounting:VO":"abcd", "unsupported": "value"}' --out plan
    test -f plan -a -s plan
}

@test "Phase4: terraform apply succeeded" {
    terraform apply plan
}

@test "Phase4: abcd unchanged as there was added unsupported property" {
    assert_properties_files "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
    assert_properties_contents "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
}

# ###########################################################################
# Phase5
# ###########################################################################
@test "Phase5: terraform plan succeeded (remove all supported properties, keep unsupported ones)" {
    rm -f plan
    terraform plan -var 'project_abcd_property_enabled=1' -var 'project_abcd_properties={"unsupported": "value", "unsupported2": "value2"}' --out plan
    test -f plan -a -s plan
}

@test "Phase5: terraform apply succeeded" {
    terraform apply plan
}

@test "Phase5: abcd supported properties kept present but emptied" {
    assert_properties_files "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
    assert_properties_contents "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
}

# ###########################################################################
# Phase6
# ###########################################################################
@test "Phase6: terraform plan succeeded (remove whole resource)" {
    rm -f plan
    terraform plan -var 'project_abcd_property_enabled=0' --out plan
    test -f plan -a -s plan
}

@test "Phase6: terraform apply succeeded" {
    terraform apply plan
}

@test "Phase6: abcd supported properties kept present but emptied" {
    assert_properties_files "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
    assert_properties_contents "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
}

# ###########################################################################
# Phase7
# ###########################################################################
@test "Phase7: terraform plan succeeded (add multiple resources)" {
    rm -f plan
    terraform plan --out plan \
      -var 'project_abcd_property_enabled=1' \
      -var 'project_abce_property_enabled=1' \
      -var 'project_abcf_property_enabled=1'
    test -f plan -a -s plan
}

@test "Phase7: terraform apply succeeded" {
    terraform apply plan
}

@test "Phase7: abcd properties emptied (from frevious phase), others not present" {
    assert_properties_files "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
    assert_properties_contents "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
}

# ###########################################################################
# Phase8
# ###########################################################################
@test "Phase8: terraform plan succeeded (fill multiple resource values)" {
    rm -f plan
    terraform plan --out plan \
      -var 'project_abcd_property_enabled=1' \
      -var 'project_abcd_properties={"VO":"abcd", "accounting:VO":"abcd", "unsupportedX": "value"}' \
      -var 'project_abce_property_enabled=1' \
      -var 'project_abce_properties={"VO":"abce", "unsupportedY": "value"}' \
      -var 'project_abcf_property_enabled=1' \
      -var 'project_abcf_properties={"accounting:VO":"abcf", "unsupportedZ": "value"}'
    test -f plan -a -s plan
}

@test "Phase8: terraform apply succeeded" {
    terraform apply plan
}

@test "Phase8: abc[def] are all filled" {
    assert_properties_files "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
    assert_properties_contents "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
}

# ###########################################################################
# Phase9
# ###########################################################################
@test "Phase9: terraform plan succeeded (changed multiple resource values)" {
    rm -f plan
    terraform plan --out plan \
      -var 'project_abcd_property_enabled=1' \
      -var 'project_abcd_properties={"unsupportedX": "value"}' \
      -var 'project_abce_property_enabled=1' \
      -var 'project_abce_properties={"accounting:VO":"abce", "unsupportedY": "value"}' \
      -var 'project_abcf_property_enabled=1' \
      -var 'project_abcf_properties={"VO":"abcf", "unsupportedZ": "value"}'
    test -f plan -a -s plan
}

@test "Phase9: terraform apply succeeded" {
    terraform apply plan
}

@test "Phase9: abc[def] are all updated accordingly" {
    assert_properties_files "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
    assert_properties_contents "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
}

# ###########################################################################
# Phase10
# ###########################################################################
@test "Phase10: terraform destroy on all properties" {
    terraform apply --destroy --auto-approve
}

@test "Phase10: abc[def] are all filled" {
    assert_properties_files "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
    assert_properties_contents "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
}

