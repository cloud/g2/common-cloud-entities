
export TERRAFORM="${TERRAFORM:-"terraform"}"

# assert_properties_files( <phase_name> )
#   assert equivalent files in ./ and ./expected-results/<phase_name>
function assert_properties_files() {
    local phase_name="$1"

    local real_files="$(ls ./1234-*.json || true)"
    local expected_files="$(ls ./expected-results/${phase_name}/1234-*.json || true)"

    test "$(echo "${real_files}" | awk -F/ '{print $NF}' | sort)" == "$(echo "${expected_files}" | awk -F/ '{print $NF}'| sort)"
}


# assert_properties_contents( <phase_name> )
#   assert property files content in ./ and ./expected-results/<phase_name>
function assert_properties_contents() {
    local phase_name="$1"
    for i_file in ./1234-*.json; do
        i_filename=$(basename "${i_file}")
        i_expected_json=$(cat "./expected-results/${phase_name}/${i_filename}" | jq -c -r -S .)
        i_real_json=$(cat "./${i_filename}" | jq -c -r -S .)
        if [ "${i_real_json}" != "${i_expected_json}" ]; then
            echo "./${i_filename} does not match ./expected-results/${phase_name}/${i_filename}"
            diff -u "./${i_filename}" "./expected-results/${phase_name}/${i_filename}"
        fi
    done
}

# get_test_phase( <test-description> )
#   get phase identification from <test-description>
function get_test_phase() {
    local description="$1"
    echo "${description}" | grep -Eo '^[^:]+'
}

