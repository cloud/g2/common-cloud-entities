terraform {
  required_providers {
    shell = {
      source = "scottwinkler/shell"
      version = "1.7.10"
    }
  }
}

provider "shell" {
    environment = {
        ACTIONS_DIR    = "${path.module}/.."
        OPENSTACK      = "${path.module}/openstack-cli-model.sh"
    }
    interpreter        = ["/bin/bash", "-c"]
    enable_parallelism = false
}


# testing resources
resource "shell_script" "ostack_project_properties_v1_1234_abcd_0001" {
    count      = var.project_abcd_property_enabled
    lifecycle_commands {
        create = file("${path.module}/../create.sh")
        read   = file("${path.module}/../read.sh")
        update = file("${path.module}/../update.sh")
        delete = file("${path.module}/../delete.sh")
    }

    environment = {
        PROJECT_ID             = "1234-abcd-0001"
        PROPERTIES             = var.project_abcd_properties
        PROVIDER_ENVTRACE_FILE = "1234-abcd-0001.vars"
    }
    working_directory = path.module
}

resource "shell_script" "ostack_project_properties_v1_1234_abce_0002" {
    count      = var.project_abce_property_enabled
    lifecycle_commands {
        create = file("${path.module}/../create.sh")
        read   = file("${path.module}/../read.sh")
        update = file("${path.module}/../update.sh")
        delete = file("${path.module}/../delete.sh")
    }

    environment = {
        PROJECT_ID             = "1234-abce-0002"
        PROPERTIES             = var.project_abce_properties
        PROVIDER_ENVTRACE_FILE = "1234-abce-0002.vars"
    }
    working_directory = path.module
}

resource "shell_script" "ostack_project_properties_v1_1234_abcf_0003" {
    count      = var.project_abcf_property_enabled
    lifecycle_commands {
        create = file("${path.module}/../create.sh")
        read   = file("${path.module}/../read.sh")
        update = file("${path.module}/../update.sh")
        delete = file("${path.module}/../delete.sh")
    }

    environment = {
        PROJECT_ID             = "1234-abcf-0003"
        PROPERTIES             = var.project_abcf_properties
        PROVIDER_ENVTRACE_FILE = "1234-abcf-0003.vars"
    }
    working_directory = path.module
}


/*
output "id" {
    value = shell_script.ostack_mapping.output["id"]
}*/
