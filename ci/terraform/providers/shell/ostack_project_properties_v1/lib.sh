
# constants
RESOURCE_STATE_SCHEMA_JSON='{"project-id": null, "project-props": null}'
SUPPORTED_OSTACK_PROJECT_PROPS=("VO" "accounting:VO")
OPENSTACK="${OPENSTACK:-"openstack"}"

# functions

# format_args( <arg> ...)
#   format input arguments
function format_args() {
  local first="true"
  for i_arg in "$@"; do
    if [ "${first}" == "false" ]; then
      echo -n ", \"${i_arg}\""
    else
      echo -n "\"${i_arg}\""
      first="false"
    fi
  done
}

JQSELECT_OSTACK_PROPS_QUERY="{$(format_args "${SUPPORTED_OSTACK_PROJECT_PROPS[@]}")}" # expected syntax: '{"VO", "accounting:VO"}'

# dump_resource_state( <previous-state> <project-id> <project-props> )
function dump_resource_state() {
    local prev_state="${1:-"${RESOURCE_STATE_SCHEMA_JSON}"}"
    local project_id="$2"
    local project_props="$3"

    echo "${prev_state}" | \
      jq -S -c --arg project_id "${project_id}" --arg project_props "${project_props}" \
        '."project-id" = $project_id | ."project-props" = $project_props'
}

# assert_nonempty_args <arg> [arg] ...
#   exit with retcode of 0 when all args are non-empty
function assert_nonempty_args() {
    for i_arg in "$@"; do
        test -n "${i_arg}"
    done
}

# at_exit()
#   at exit callback function handling stderr state logging
function at_exit() {
    local step_name="${STEP_NAME:-"unknown"}"

    if [ "${step_name}" != "Succeeded" ]; then
        echo "Step \"${step_name}\" failed!" >&2
        set | grep -E '^(IN|ID|STEP_NAME|mapping_).*=' >&2
        exit "${STEP_FAIL_EXIT_CODE:-10}"
    fi
}

# get_tf_desired_ostack_project_props( <tf-desired-project-props-state-json> )
#   returns Terraform desired ostack project properties as normalized json
function get_tf_desired_ostack_project_props() {
    local tf_desired_ostack_project_props_state_json="$1"
    echo "${tf_desired_ostack_project_props_state_json}" | jq -c -S .
}

# get_ostack_project_props( <ostack-project-id> )
#   returns Terraform desired ostack project properties as normalized json
function get_ostack_project_props() {
    local ostack_project_id="$1"
    ${OPENSTACK} project show ${ostack_project_id} -fjson | \
      jq -S -c "${JQSELECT_OSTACK_PROPS_QUERY}"
}

# get_ostack_project_property( <ostack-project-props-json> <property-name> )
function get_ostack_project_property() {
    local ostack_project_props="$1"
    local ostack_project_property_name="$2"

    echo "${ostack_project_props}" | jq -r ".\"${ostack_project_property_name}\""
}

# update_ostack_props ( <ostack-project-id>
#                       <expected-tf-ostack-project-props-json>
#                       <real-ostack-project-props-json> )
function update_ostack_props() {
    local ostack_project_id="$1"
    local exp_tf_ostack_project_props="$2"
    local real_ostack_project_props="$3"

    for i_ostack_project_property in "${SUPPORTED_OSTACK_PROJECT_PROPS[@]}"; do
        i_tf_exp_ostack_prj_prop="$(get_ostack_project_property "${exp_tf_ostack_project_props}" \
                                                                "${i_ostack_project_property}")"
        i_real_ostack_prj_prop="$(get_ostack_project_property "${real_ostack_project_props}" \
                                                              "${i_ostack_project_property}")"

        if [ "${i_tf_exp_ostack_prj_prop}" != "${i_real_ostack_prj_prop}" ]; then
            if [ "${i_real_ostack_prj_prop}" != "null" -a "${i_tf_exp_ostack_prj_prop}" == "null" ]; then
                # as ostack project property cannot be deleted at least we empty it
                ${OPENSTACK} project set --property "${i_ostack_project_property}=" "${ostack_project_id}"
            elif [ "${i_real_ostack_prj_prop}" != "" -a "${i_tf_exp_ostack_prj_prop}" == "null" ]; then
                # as ostack project property cannot be deleted, tf may not have it but in reality that equals to propery empty
                true
            else
                ${OPENSTACK} project set --property "${i_ostack_project_property}=${i_tf_exp_ostack_prj_prop}" "${ostack_project_id}"
            fi
        fi
    done
}

# delete_ostack_props ( <ostack-project-id> <real-ostack-project-props-json> )
function delete_ostack_props() {
    local ostack_project_id="$1"
    local real_ostack_project_props="$2"

    for i_ostack_project_property in "${SUPPORTED_OSTACK_PROJECT_PROPS[@]}"; do
        i_real_ostack_prj_prop="$(get_ostack_project_property "${real_ostack_project_props}" \
                                                              "${i_ostack_project_property}")"

        if [ "${i_real_ostack_prj_prop}" != "null" -a "${i_real_ostack_prj_prop}" != "" ]; then
            ${OPENSTACK} project set --property "${i_ostack_project_property}=" "${ostack_project_id}"
        fi
    done
}

# dump_provider_state( <provider-envtrace-file> <message> )
function dump_provider_state() {
    local provider_envtrace_file="${1:-""}"
    local message="${2:-""}"

    if [ -n "${provider_envtrace_file}" ]; then
        echo -e "\n$(date +%Y-%m-%dT%H:%M:%S.%N) ${message}" >> "${provider_envtrace_file}"
        set | grep -E "^[a-zA-Z_][a-zA-Z0-9_]+=" | awk '{print "  " $0}' >> "${provider_envtrace_file}"
    fi
}

# resource CRUD methods

# resource_read (<in-state> <project-id>)
function resource_read() {
    local in_state="$1"
    local project_id="$2"

    STEP_NAME="Assert input arguments"
    assert_nonempty_args "${project_id}"

    STEP_NAME="Read OpenStack project properties"
    local ostack_project_props="$(get_ostack_project_props "${project_id}")"

    STEP_NAME="Dump TF resource state"
    dump_resource_state "${in_state}" "${project_id}" "${ostack_project_props}"

    STEP_NAME="Dump TF provider state"
    dump_provider_state "${PROVIDER_ENVTRACE_FILE}" "${FUNCNAME[0]}($(format_args "$@"))"

    STEP_NAME="Succeeded"
}

# resource_create( <project-id> <properties> )
function resource_create() {
    local project_id="$1"
    local properties="$2"

    STEP_NAME="Assert input arguments"
    assert_nonempty_args "${project_id}" "${properties}"

    STEP_NAME="Read input terraform (desired) project properties"
    local tf_project_props="$(get_tf_desired_ostack_project_props "${properties}")"

    STEP_NAME="Read OpenStack project properties (before change)"
    local ostack_project_props_pre="$(get_ostack_project_props "${project_id}")"

    STEP_NAME="Create OpenStack project properties"
    update_ostack_props "${project_id}" "${tf_project_props}" "${ostack_project_props_pre}"

    STEP_NAME="Read OpenStack project properties (after change)"
    local ostack_project_props_post="$(get_ostack_project_props "${project_id}")"

    STEP_NAME="Dump TF provider state"
    dump_resource_state "" "${project_id}" "${ostack_project_props_post}"

    dump_provider_state "${PROVIDER_ENVTRACE_FILE}" "${FUNCNAME[0]}($(format_args "$@"))"

    STEP_NAME="Succeeded"
}

# resource_update( <in-state> <project-id> <properties> )
function resource_update() {
    local in_state="$1"
    local project_id="$2"
    local properties="$3"

    STEP_NAME="Assert input arguments"
    assert_nonempty_args "${project_id}" "${properties}"

    STEP_NAME="Read input terraform (desired) project properties"
    local tf_desired_project_props="$(get_tf_desired_ostack_project_props "${properties}")"

    STEP_NAME="Read (real) OpenStack project properties"
    local ostack_project_props_pre="$(get_ostack_project_props ${project_id})"

    STEP_NAME="Create OpenStack project properties (if needed)"
    if [ "${tf_desired_project_props}" != "${ostack_project_props_pre}" ]; then
        update_ostack_props "${project_id}" "${tf_desired_project_props}" "${ostack_project_props_pre}"
    fi

    STEP_NAME="Read OpenStack project properties"
    local ostack_project_props_post="$(get_ostack_project_props ${project_id})"

    STEP_NAME="Dump TF resource state"
    dump_resource_state "${in_state}" "${project_id}" "${ostack_project_props_post}"

    STEP_NAME="Dump TF provider state"
    dump_provider_state "${PROVIDER_ENVTRACE_FILE}" "${FUNCNAME[0]}($(format_args "$@"))"

    STEP_NAME="Succeeded"
}

# resource_delete( <in-state> <project-id> )
function resource_delete() {
    local in_state="$1"
    local project_id="$2"

    STEP_NAME="Assert input arguments"
    assert_nonempty_args "${project_id}"

    STEP_NAME="Read OpenStack project properties"
    local ostack_project_props="$(get_ostack_project_props "${project_id}")"

    STEP_NAME="Delete openstack project properties"
    delete_ostack_props "${project_id}" "${ostack_project_props}"

    STEP_NAME="Dump TF resource state"
    dump_resource_state "${in_state}" "${project_id}" "{ }"

    STEP_NAME="Dump TF provider state"
    dump_provider_state "${PROVIDER_ENVTRACE_FILE}" "${FUNCNAME[0]}($(format_args "$@"))"

    STEP_NAME="Succeeded"
}
