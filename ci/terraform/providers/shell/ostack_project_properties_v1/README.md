# ostack_project_properties_v1 custom terraform shell resource provider

OpenStack project property resource provider with shared library, example terraform code and basic unit testing.

There is custom terraform provider as official openstack one doews not support ostack project properties.
OpenStack project properties are deprecated in favor of project tags.
Unfortunatelly EGI still uses OpenStack project properties.


## Goal

Create declarative description for OpenStack project properties used by EGI for accounting (to be able to add project properties `VO` and `accounting:VO`).

## Implementation

Terraform shell based provider relies on OpenStack command-line client app.


### Reading ostack project properties

```sh
freznicek@lenovo-t14 ~ 0]$ openstack project show 2e4386fd14af4fb2bef5eb716af4fdaa -fjson | jq '{VO, "accounting:VO"}'
{
  "VO": "X",
  "accounting:VO": "X"
}
```

### reading ostack project properties from project-quota-acl object / file

```sh
[freznicek@lenovo-t14 egi_eu 0]$ yq .project envri-vre.egi.eu.yaml -ojson | jq .properties
{
  "accounting:VO": "envri-vre.egi.eu",
  "VO": "envri-vre.egi.eu"
}
```

### adding / changing ostack project property

```sh
openstack project set --property accounting:VO=X 2e4386fd14af4fb2bef5eb716af4fdaa

```
### deleting ostack project property

Deletion of the ostack project property is not possible via API, therefore provider empties the property:

```sh
openstack project set --property accounting:VO= 2e4386fd14af4fb2bef5eb716af4fdaa
```
