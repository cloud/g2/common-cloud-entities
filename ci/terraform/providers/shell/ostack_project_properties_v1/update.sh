set -eo pipefail

source ${ACTIONS_DIR:-.}/lib.sh

trap at_exit EXIT

resource_update "$(cat)" "${PROJECT_ID}" "${PROPERTIES}"
