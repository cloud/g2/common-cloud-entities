
# constants

# functions

# dump_resource_state( <previous-state> <name> <file-content> )
function dump_resource_state() {
    local prev_state="${1:-"{}"}"
    local name="$2"
    local file_content="$3"

    echo "${prev_state}" | \
      jq -c --arg name "${name}" --arg file_content "${file_content}" '.name = $name | ."file-content" = $file_content'
}

# assert_nonempty_args <arg> [arg] ...
#   exit with retcode of 0 when all args are non-empty
function assert_nonempty_args() {
    for i_arg in "$@"; do
        test -n "${i_arg}"
    done
}

# at_exit()
#   at exit callback function handling stderr state logging
function at_exit() {
    local step_name="${STEP_NAME:-"unknown"}"

    if [ "${step_name}" != "Succeeded" ]; then
        echo "Step \"${step_name}\" failed!" >&2
        set | grep -E '^(IN|ID|STEP_NAME|mapping_).*=' >&2
        exit "${STEP_FAIL_EXIT_CODE:-10}"
    fi
}

# dump_provider_state( <provider-envtrace-file> <message> )
function dump_provider_state() {
    local provider_envtrace_file="${1:-""}"
    local message="${2:-""}"

    if [ -n "${provider_envtrace_file}" ]; then
        echo -e "\n$(date +%Y%m%dT%H%M%S) ${message}"
        set | grep -E "^[a-zA-Z_][a-zA-Z0-9_]+=" | awk '{print "  " $0}'
    fi
}

