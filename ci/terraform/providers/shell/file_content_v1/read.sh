#!/bin/bash

set -eo pipefail

source ${ACTIONS_DIR:-.}/lib.sh

trap at_exit EXIT

STEP_NAME="Read TF provider stdin arguments"
IN="$(cat)"

STEP_NAME="Assert input arguments"
assert_nonempty_args "${NAME}"

STEP_NAME="Read file content (after change)"
file_content="$(cat "${NAME}.data")"

STEP_NAME="Dump TF provider state"
dump_resource_state "${IN}" "${NAME}" "${file_content}"

dump_provider_state "${PROVIDER_ENVTRACE_FILE}" "read $@" >> "${PROVIDER_ENVTRACE_FILE}"

STEP_NAME="Succeeded"
