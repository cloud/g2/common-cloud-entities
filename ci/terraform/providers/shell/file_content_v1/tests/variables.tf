
variable "file_abcd_enabled" {
  default = 0
  description = "project_abcd_property resource enable (0/1 ~ disabled/enabled)"
}

variable "file_abcd_content" {
  default = ""
  description = "project abcd properties"
}


variable "file_abce_enabled" {
  default = 0
  description = "project abce property resource enable (0/1 ~ disabled/enabled)"
}

variable "file_abce_content" {
  default = ""
  description = "project abce properties"
}

variable "file_abcf_enabled" {
  default = 0
  description = "project abcf property resource enable (0/1 ~ disabled/enabled)"
}

variable "file_abcf_content" {
  default = ""
  description = "project abcf properties"
}
