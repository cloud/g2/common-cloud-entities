terraform {
  required_providers {
    shell = {
      source = "scottwinkler/shell"
      version = "1.7.10"
    }
  }
}

provider "shell" {
    environment = {
        ACTIONS_DIR = "${path.module}/.."
    }
    interpreter = ["/bin/bash", "-c"]
    enable_parallelism = false
}


# testing resources
resource "shell_script" "file_content_v1_1234_abcd_0001" {
    count      = var.file_abcd_enabled
    lifecycle_commands {
        create = file("${path.module}/../create.sh")
        read   = file("${path.module}/../read.sh")
        update = file("${path.module}/../update.sh")
        delete = file("${path.module}/../delete.sh")
    }

    environment = {
        NAME     = "1234-abcd-0001"
        CONTENT  = var.file_abcd_content
        PROVIDER_ENVTRACE_FILE = "1234-abcd-0001.vars"
    }
    working_directory = path.module
}

resource "shell_script" "file_content_v1_1234_abce_0002" {
    count      = var.file_abce_enabled
    lifecycle_commands {
        create = file("${path.module}/../create.sh")
        read   = file("${path.module}/../read.sh")
        update = file("${path.module}/../update.sh")
        delete = file("${path.module}/../delete.sh")
    }

    environment = {
        NAME     = "1234-abce-0002"
        CONTENT  = var.file_abce_content
        PROVIDER_ENVTRACE_FILE = "1234-abce-0002.vars"
    }
    working_directory = path.module
}

resource "shell_script" "file_content_v1_1234_abcf_0003" {
    count      = var.file_abcf_enabled
    lifecycle_commands {
        create = file("${path.module}/../create.sh")
        read   = file("${path.module}/../read.sh")
        update = file("${path.module}/../update.sh")
        delete = file("${path.module}/../delete.sh")
    }

    environment = {
        NAME     = "1234-abcf-0003"
        CONTENT  = var.file_abcf_content
        PROVIDER_ENVTRACE_FILE = "1234-abcf-0003.vars"
    }
    working_directory = path.module
}


/*
output "id" {
    value = shell_script.ostack_mapping.output["id"]
}*/
