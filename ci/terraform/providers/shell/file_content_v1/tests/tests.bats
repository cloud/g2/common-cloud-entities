#!/usr/bin/env bats

load test-lib.sh

# Setup and Teardown Functions
# ###########################################################################
setup() {
    pushd "$BATS_TEST_DIRNAME"
}

teardown() {
    popd
}


# Prelude
# ###########################################################################
@test "environment is cleaned up (we are starting from scratch)" {
    rm -rf ./.terraform
    rm -f ./.terraform.lock.hcl ./terraform.tfstate ./plan ./1234-abc*
}

@test "terraform client installed" {
    ${TERRAFORM} --help
}

@test "terraform initialization succeeded" {
    ${TERRAFORM} init
}

@test "terraform infrastructure declaration is valid (validated)" {
    ${TERRAFORM} validate
}

# ###########################################################################
# Phase0
# ###########################################################################
@test "Phase0: terraform plan succeeded" {
    ${TERRAFORM} plan --out plan
    test -f plan -a -s plan
}

@test "Phase0: terraform apply succeeded" {
    ${TERRAFORM} apply plan
}

@test "Phase0: no files produced yet" {
    assert_properties_files "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
}

# ###########################################################################
# Phase1
# ###########################################################################
@test "Phase1: terraform plan succeeded (added empty file content resource)" {
    rm -f plan
    ${TERRAFORM} plan -var 'file_abcd_enabled=1' --out plan
    test -f plan -a -s plan
}

@test "Phase1: terraform apply succeeded" {
    ${TERRAFORM} apply plan
}

@test "Phase1: abcd file exists, empty" {
    assert_properties_files "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
}

# ###########################################################################
# Phase2
# ###########################################################################
@test "Phase2: terraform plan succeeded (added file content)" {
    rm -f plan
    ${TERRAFORM} plan -var 'file_abcd_enabled=1' -var 'file_abcd_content={"VO":"abcd"}' --out plan
    test -f plan -a -s plan
}

@test "Phase2: terraform apply succeeded" {
    ${TERRAFORM} apply plan
}

@test "Phase2: abcd file exists, non-empty" {
    assert_properties_files "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
    assert_properties_contents "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
}

# ###########################################################################
# Phase3
# ###########################################################################
@test "Phase3: terraform plan succeeded (changed file content)" {
    rm -f plan
    ${TERRAFORM} plan -var 'file_abcd_enabled=1' -var 'file_abcd_content={"VO":"abcd", "accounting:VO":"abcd"}' --out plan
    test -f plan -a -s plan
}

@test "Phase3: terraform apply succeeded" {
    ${TERRAFORM} apply plan
}

@test "Phase3: abcd file exists, non-empty, content changed" {
    assert_properties_files "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
    assert_properties_contents "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
}

# ###########################################################################
# Phase6
# ###########################################################################
@test "Phase6: terraform plan succeeded (remove whole resource)" {
    rm -f plan
    ${TERRAFORM} plan -var 'file_abcd_enabled=0' --out plan
    test -f plan -a -s plan
}

@test "Phase6: terraform apply succeeded" {
    ${TERRAFORM} apply plan
}

@test "Phase6: abcd file does not exist anymore" {
    assert_properties_files "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
    #assert_properties_contents "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
}

# ###########################################################################
# Phase7
# ###########################################################################
@test "Phase7: terraform plan succeeded (add multiple empty file resources)" {
    rm -f plan
    ${TERRAFORM} plan --out plan \
      -var 'file_abcd_enabled=1' \
      -var 'file_abce_enabled=1' \
      -var 'file_abcf_enabled=1'
    test -f plan -a -s plan
}

@test "Phase7: terraform apply succeeded" {
    ${TERRAFORM} apply plan
}

@test "Phase7: multiple empty files" {
    assert_properties_files "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
    assert_properties_contents "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
}

# ###########################################################################
# Phase8
# ###########################################################################
@test "Phase8: terraform plan succeeded (multiple changed contents)" {
    rm -f plan
    ${TERRAFORM} plan --out plan \
      -var 'file_abcd_enabled=1' \
      -var 'file_abcd_content={"VO":"abcd", "accounting:VO":"abcd", "unsupportedX": "value"}' \
      -var 'file_abce_enabled=1' \
      -var 'file_abce_content={"VO":"abce", "unsupportedY": "value"}' \
      -var 'file_abcf_enabled=1' \
      -var 'file_abcf_content={"accounting:VO":"abcf", "unsupportedZ": "value"}'
    test -f plan -a -s plan
}

@test "Phase8: terraform apply succeeded" {
    ${TERRAFORM} apply plan
}

@test "Phase8: abc[def] are changed, non-empty" {
    assert_properties_files "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
    assert_properties_contents "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
}

# ###########################################################################
# Phase9
# ###########################################################################
@test "Phase9: terraform plan succeeded (multiple changed contents)" {
    rm -f plan
    ${TERRAFORM} plan --out plan \
      -var 'file_abcd_enabled=1' \
      -var 'file_abcd_content={"unsupportedX": "value"}' \
      -var 'file_abce_enabled=1' \
      -var 'file_abce_content={"accounting:VO":"abce", "unsupportedY": "value"}' \
      -var 'file_abcf_enabled=1' \
      -var 'file_abcf_content={"VO":"abcf", "unsupportedZ": "value"}'
    test -f plan -a -s plan
}

@test "Phase9: terraform apply succeeded" {
    ${TERRAFORM} apply plan
}

@test "Phase9: abc[def] are changed, non-empty" {
    assert_properties_files "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
    assert_properties_contents "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
}

# ###########################################################################
# Phase10
# ###########################################################################
@test "Phase10: terraform destroy on all properties" {
    ${TERRAFORM} apply --destroy --auto-approve
}

@test "Phase10: abc[def] removed, not exists anymore" {
    assert_properties_files "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
    #assert_properties_contents "$(get_test_phase "${BATS_TEST_DESCRIPTION}")"
}

