#!/bin/bash

set -eo pipefail

source ${ACTIONS_DIR:-.}/lib.sh

STEP_NAME="Read TF provider stdin arguments"
IN="$(cat)"

STEP_NAME="Assert input arguments"
assert_nonempty_args "${NAME}"

STEP_NAME="Delete file"
rm -f "${NAME}.data"

STEP_NAME="Dump TF provider state"
dump_resource_state "${IN}" "${NAME}" ""

dump_provider_state "${PROVIDER_ENVTRACE_FILE}" "delete $@" >> "${PROVIDER_ENVTRACE_FILE}"

STEP_NAME="Succeeded"
