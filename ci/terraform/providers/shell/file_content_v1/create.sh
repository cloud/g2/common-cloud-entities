#!/bin/bash

set -eo pipefail

source ${ACTIONS_DIR:-.}/lib.sh

trap at_exit EXIT

STEP_NAME="Assert input arguments"
assert_nonempty_args "${NAME}"

STEP_NAME="Read file content (before change)"
test -f "${NAME}.data" || \
  touch "${NAME}.data"
file_content_pre="$(cat "${NAME}.data")"

STEP_NAME="Create OpenStack project properties"
if [ "${CONTENT}" != "${file_content_pre}" ]; then
    echo -n "${CONTENT}" > "${NAME}.data"
fi

STEP_NAME="Read file content (after change)"
file_content="$(cat "${NAME}.data")"

STEP_NAME="Dump TF provider state"
dump_resource_state "" "${NAME}" "${file_content}"

dump_provider_state "${PROVIDER_ENVTRACE_FILE}" "create $@" >> "${PROVIDER_ENVTRACE_FILE}"

STEP_NAME="Succeeded"
