
# constants

# functions

# format_args( <arg> ...)
#   format input arguments
function format_args() {
  local first="true"
  for i_arg in "$@"; do
    if [ "${first}" == "false" ]; then
      echo -n ", \"${i_arg}\""
    else
      echo -n "\"${i_arg}\""
      first="false"
    fi
  done
}

# dump_resource_state( <previous-state> <name> <file-content> )
function dump_resource_state() {
    local prev_state="${1:-"{}"}"
    local name="$2"
    local file_content="$3"

    echo "${prev_state}" | \
      jq -c --arg name "${name}" --arg file_content "${file_content}" '.name = $name | ."file-content" = $file_content'
}

# assert_nonempty_args <arg> [arg] ...
#   exit with retcode of 0 when all args are non-empty
function assert_nonempty_args() {
    for i_arg in "$@"; do
        test -n "${i_arg}"
    done
}

# at_exit()
#   at exit callback function handling stderr state logging
function at_exit() {
    local step_name="${STEP_NAME:-"unknown"}"

    if [ "${step_name}" != "Succeeded" ]; then
        echo "Step \"${step_name}\" failed!" >&2
        set | grep -E '^(IN|ID|STEP_NAME|mapping_).*=' >&2
        exit "${STEP_FAIL_EXIT_CODE:-10}"
    fi
}

# dump_provider_state( <provider-envtrace-file> <message> )
function dump_provider_state() {
    local provider_envtrace_file="${1:-""}"
    local message="${2:-""}"

    if [ -n "${provider_envtrace_file}" ]; then
        echo -e "\n$(date +%Y%m%dT%H%M%S) ${message}"
        set | grep -E "^[a-zA-Z_][a-zA-Z0-9_]+=" | awk '{print "  " $0}'
    fi
}

function resource_read() {
    local in_state="$1"
    local name="$2"

    STEP_NAME="Assert input arguments"
    assert_nonempty_args "${name}"

    STEP_NAME="Read file content (after change)"
    file_content="$(cat "${name}.data")"

    STEP_NAME="Dump TF provider state"
    dump_resource_state "${in_state}" "${name}" "${file_content}"

    dump_provider_state "${PROVIDER_ENVTRACE_FILE}" "${FUNCNAME[0]}($(format_args "$@"))" >> "${PROVIDER_ENVTRACE_FILE}"

    STEP_NAME="Succeeded"
}

function resource_create() {
    local name="$1"
    local content="$2"

    STEP_NAME="Assert input arguments"
    assert_nonempty_args "${name}"

    STEP_NAME="Create OpenStack project properties"
    echo -n "${content}" > "${name}.data"

    STEP_NAME="Read file content (after change)"
    file_content="$(cat "${name}.data")"

    STEP_NAME="Dump TF provider state"
    dump_resource_state "" "${name}" "${file_content}"

    dump_provider_state "${PROVIDER_ENVTRACE_FILE}" "${FUNCNAME[0]}($(format_args "$@"))" >> "${PROVIDER_ENVTRACE_FILE}"

    STEP_NAME="Succeeded"
}

function resource_update() {
    local in_state="$1"
    local name="$2"
    local content="$3"

    STEP_NAME="Assert input arguments"
    assert_nonempty_args "${name}"

    STEP_NAME="Create OpenStack project properties"
    echo -n "${content}" > "${name}.data"

    STEP_NAME="Read file content (after change)"
    file_content="$(cat "${name}.data")"

    STEP_NAME="Dump TF provider state"
    dump_resource_state "${in_state}" "${name}" "${file_content}"

    dump_provider_state "${PROVIDER_ENVTRACE_FILE}" "${FUNCNAME[0]}($(format_args "$@"))" >> "${PROVIDER_ENVTRACE_FILE}"

    STEP_NAME="Succeeded"
}

function resource_delete() {
    local in_state="$1"
    local name="$2"

    STEP_NAME="Assert input arguments"
    assert_nonempty_args "${name}"

    STEP_NAME="Delete file"
    rm -f "${name}.data"

    STEP_NAME="Dump TF provider state"
    dump_resource_state "${in_state}" "${name}" ""

    dump_provider_state "${PROVIDER_ENVTRACE_FILE}" "${FUNCNAME[0]}($(format_args "$@"))" >> "${PROVIDER_ENVTRACE_FILE}"

    STEP_NAME="Succeeded"
}

